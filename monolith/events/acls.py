import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    search_query = f"{city}&{state}&landmark&per_page=1"
    url = f"https://api.pexels.com/v1/search?query={search_query}"
    response = requests.get(url, headers=headers)
    parsed_response = json.loads(response.content)
    picture_url = parsed_response["photos"][0]["src"]["original"]
    return picture_url


def get_weather_data(city, state):
    loc_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},us&appid={OPEN_WEATHER_API_KEY}"
    response1 = requests.get(loc_url)
    first_parsed_response = json.loads(response1.content)

    if not first_parsed_response:
        return None

    lat = first_parsed_response[0]["lat"]
    lon = first_parsed_response[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url)
    second_parsed_response = json.loads(response.content)

    if not second_parsed_response:
        return None

    weather = {
        "temp": second_parsed_response["main"]["temp"],
        "description": second_parsed_response["weather"][0]["description"],
    }
    return weather
